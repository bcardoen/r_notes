### R Notes

#### R building
```bash
$./configure --enable-R-shlib --with-blas --with-lapack && make -j
```

#### Gaussian Mixture
Let X be column vector of values.
```R
install.packages("mclust")
library(mclust)
h = hist(t(X), breaks="Freedman-Diaconis")
yBIC = mclustBIC(t(X), modelNames="V")
yModel = mclustModel(t(X), yBIC)
```
